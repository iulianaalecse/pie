<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', 'HomeController@index')->name('dashboard');

// Pentru creare controller rulati comanda:
// php artisan make:controller NumeController --resource

// Cand adaugati fisiere noi in proiect nu uiti sa le adaugati si in git
// Daca sunt rosii nu sunt adaugate in git
// Cand sunt verzi sau albastre sunt in git
// Pentru a le adauga in git comanda: git add .
// Sau click dreapta pe fisier -> Git -> Add

// Ani - Years
Route::get('/year', 'YearController@index')->name('year.index');
Route::get('/year/create', 'YearController@create')->name('year.create');
Route::post('/year/store', 'YearController@store')->name('year.store');
Route::get('/year/edit/{id}', 'YearController@edit')->name('year.edit');
Route::post('/year/update/{id}', 'YearController@update')->name('year.update');