<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisciplineModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discipline_modules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hours');
            $table->unsignedInteger('modulable_id');
            $table->string('modulable_type');
            $table->unsignedInteger('teacher_id');
            $table->unsignedInteger('module_id');
            $table->unsignedInteger('year_id');
            $table->unsignedInteger('group_id');
            $table->unsignedInteger('subgroup_id');
            $table->unsignedInteger('rotation_id');
            $table->timestamps();

            $table->foreign('teacher_id')->references('id')->on('teachers');
            $table->foreign('module_id')->references('id')->on('modules');
            $table->foreign('year_id')->references('id')->on('years');
            $table->foreign('group_id')->references('id')->on('groups');
            $table->foreign('subgroup_id')->references('id')->on('subgroups');
            $table->foreign('rotation_id')->references('id')->on('rotations');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discipline_modules');
    }
}
