<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisciplineModuleWeeksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discipline_module_weeks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('discipline_module_id');
            $table->unsignedInteger('week_id');
            $table->unsignedInteger('status_id');
            $table->timestamps();

            $table->foreign('discipline_module_id')->references('id')->on('discipline_modules');
            $table->foreign('week_id')->references('id')->on('weeks');
            $table->foreign('status_id')->references('id')->on('statuses');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discipline_module_weeks');
    }
}
