<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(CollegeYearsTableSeeder::class);
         $this->call(ModulesTableSeeder::class);
         $this->call(RightsTableSeeder::class);
         $this->call(RotationsTableSeeder::class);
         $this->call(StatusesTableSeeder::class);
         $this->call(UsersTableSeeder::class);
    }
}
