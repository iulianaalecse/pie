@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-sm-4">
            <div class="dashboard-card">
                <h1>Profesori</h1>
                <p>Lorem ipsum dolor sit amet consectetur adipiscing.</p>
                <div>
                    <a href="">Acceseaza</a>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="dashboard-card">
                <h1>Studenti</h1>
                <p>Lorem ipsum dolor sit amet consectetur adipiscing.</p>
                <div>
                    <a href="">Acceseaza</a>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="dashboard-card">
                <h1>Grupe</h1>
                <p>Lorem ipsum dolor sit amet consectetur adipiscing.</p>
                <div>
                    <a href="">Acceseaza</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-15">
        <div class="col-sm-4">
            <h1>Butoane</h1>
            <button class="btn ">Default</button>
            <button class="btn blue">Blue</button>
            <button class="btn turquoise">Turquoise</button>
            <button class="btn green">Green</button>
        </div>
        <div class="col-sm-4">
            <h1>Formular</h1>
            <input />
            <div class="mt-15"></div>
            <select>
                <option>Option 1</option>
                <option>Option 2</option>
                <option>Option 3</option>
            </select>
            <textarea class="mt-15" rows="3" placeholder="Mesajul dvs..."></textarea>
            <div class="mt-15">
                <ul class="unstyled centered">
                    <li>
                        <input class="styled-checkbox" id="styled-checkbox-1" type="checkbox" value="value1">
                        <label for="styled-checkbox-1">Checkbox</label>
                    </li>
                    <li>
                        <input class="styled-checkbox" id="styled-checkbox-2" type="checkbox" value="value2" checked>
                        <label for="styled-checkbox-2">CSS Only</label>
                    </li>
                    <li>
                        <input class="styled-checkbox" id="styled-checkbox-3" type="checkbox" value="value3" disabled>
                        <label for="styled-checkbox-3">A disabled checkbox</label>
                    </li>
                    <li>
                        <input class="styled-checkbox" id="styled-checkbox-4" type="checkbox" value="value4">
                        <label for="styled-checkbox-4">Fourth option</label>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-sm-4">
            <h1>Tabel</h1>
            <table class="table">
                <thead>
                <tr>
                    <th>Firstname</th>
                    <th>Lastname</th>
                    <th>Email</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>John</td>
                    <td>Doe</td>
                    <td>john@example.com</td>
                </tr>
                <tr>
                    <td>Mary</td>
                    <td>Moe</td>
                    <td>mary@example.com</td>
                </tr>
                <tr>
                    <td>July</td>
                    <td>Dooley</td>
                    <td>july@example.com</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

@endsection
