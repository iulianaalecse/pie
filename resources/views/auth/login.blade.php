<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Gestiune absente | Login</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('css/custom.css')}}" rel="stylesheet">

</head>
<body>
<div id="loginpage">
    <img id="loginbg" src="{{asset('images/udj.jpg')}}"/>
    <div class="bg-overlay">
        <div id="login-box">
            <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}

                <h1 class="school-book green-text">Login</h1>
                <input type="text" placeholder="Email" name="email" />
                <input type="password" placeholder="Parola" name="password"/>
                <button type="submit" class="btn green">Sign in</button>
            </form>
        </div>
    </div>
</div>

<script src="{{asset('js/jquery-3.2.1.js')}}"></script>
<script src="{{asset('js/bootstrap.js')}}"></script>
<script src="{{asset('js/custom.js')}}"></script>
</body>
</html>