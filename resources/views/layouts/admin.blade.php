<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('css/custom.css')}}" rel="stylesheet">
    <link href="{{asset('css/toastr.css')}}" rel="stylesheet">

    @yield('styles')
</head>
<body>

@include('partials.navbar')
@include('partials.side_menu')

<div id="page-content">
    @yield('content')
</div>

<script src="{{asset('js/jquery-3.2.1.js')}}"></script>
<script src="{{asset('js/bootstrap.js')}}"></script>
<script src="{{asset('js/custom.js')}}"></script>
<script src="{{asset('js/toastr.js')}}"></script>
<script>

    @if(!is_array(session('errors')) && get_class(session('errors')) == 'Illuminate\Support\ViewErrorBag')
            {{changeSessionToNormal()}}
            @endif
            @if(session('errors'))
            @foreach(session('errors') as $notification)
        toastr["error"]("{{$notification}}", 'Eroare!');
    @endforeach
            @endif
            @if(session('warnings'))
            @foreach(session('warnings') as $notification)
        toastr["warning"]("{{$notification}}", "Atentie!");
    @endforeach
            @endif
            @if(session('successes'))
            @foreach(session('successes') as $notification)
        toastr["success"]("{{$notification}}", "Succes!");
    @endforeach
            @endif
            @if(session('infos'))
            @foreach(session('infos') as $notification)
        toastr["info"]("{{$notification}}", "Info!");
    @endforeach
            @endif

            @if(session('notifies'))
            @foreach(session('notifies') as $notification)
        toastr["info"]("{{$notification}}", "Notificare!");
    @endforeach
            @endif

            @if(session('error_fields'))
            @foreach(session('error_fields') as $notification)
        toastr["error"]("Completati campul \"{{ucfirst($notification)}}\"", "Eroare!");
    @endforeach
    @endif
</script>
@yield('scripts')
</body>
</html>