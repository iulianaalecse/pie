@extends('layouts.admin')

@section('title')
    Adauga An
@stop


@section('content')
    <h2>Adauga an</h2>

    <form action="{{route('year.store')}}" method="post">
        <div class="col-md-12">

            {{ csrf_field() }}
            <label for="label" class="control-label">Denumire</label>
            <input type="text" name="label" id="label">
            <label for="start" class="control-label">Data de incepere</label>
            <input type="date" id="start" name="start" class="datepicker">
            <label for="start" class="control-label">Data de sfarsit</label>
            <input type="date" id="start" name="end" class="datepicker">
        </div>
        <div class="col-md-12">
            <button type="submit" class="btn green pull-right mt-15">Salveaza</button>
        </div>
    </form>

@stop
