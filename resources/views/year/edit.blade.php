@extends('layouts.admin')

@section('title')
    Editeaza Anul {{$year->label}}
@stop


@section('content')
    <h2>Editeaza Anul {{$year->label}}</h2>

    <form action="{{route('year.update', ['id' => $year->id])}}" method="post">
        <div class="col-md-12">

            {{ csrf_field() }}
            <label for="label" class="control-label">Denumire</label>
            <input type="text" name="label" id="label" value="{{$year->label}}">
            <label for="start" class="control-label">Data de incepere</label>
            <input type="date" id="start" name="start" class="datepicker" value="{{$year->start}}">
            <label for="start" class="control-label">Data de sfarsit</label>
            <input type="date" id="start" name="end" class="datepicker" value="{{$year->end}}">
        </div>
        <div class="col-md-12">
            <button type="submit" class="btn green pull-right mt-15">Salveaza</button>
        </div>
    </form>

@stop
