@extends('layouts.admin')

@section('title')
    Ani
@stop

@section('content')

    <h1>Ani</h1>
    <a href="{{route('year.create')}}" class="btn pull-right mb-10"><i class="fa fa-plus"></i> Adauga</a>
    <table class="table">
        <thead>
        <tr>
            <th>Label</th>
            <th>Durata</th>
            <th>Actiuni</th>
            <th>Detalii</th>
        </tr>
        </thead>
        <tbody>
        @foreach($years as $year)
        <tr>
            <td>{{$year->label}}</td>
            <td>{{$year->start}} - {{$year->end}}</td>
            <td>
                <a href="{{route('year.edit', ['id' => $year->id])}}" class="btn blue"><i class="fa fa-pencil-square"></i> Editeaza</a>
            </td>
            <td>
                Creat la: {{$year->created_at}} <br>
                Actualizat la : {{$year->updated_at}}
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>


@stop